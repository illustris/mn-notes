* Git is cool
** Used to be distributed, isn't /really/~~ anymore
*** Blessed repository
*** Developer public copies - forks
*** Developer private copies
 These are pushed to public copies
** Fully offline
* Alternatives: svn, fossil
* git stuff - commands and config
  - init
  - clone
  - status
  - add
    - =git add .= doesn't remove files that are removed from the working tree. =git add -A= does.
  - rm
  - gitk
  - commit
    - can be signed with ssh key, using =-S=
  - log
  - reflog
  - pull
    - equivalent to =git fetch && git merge=
    - =pull --rebase= rebases instead of merging
  - branch
  - merge
  - checkout
  - show
  - reset
    - soft, hard
    - address nth parents like ~n or ^n
      - exercise for the reader: difference between ~ and ^.
  - tag
    - annotated tag
  - fancy changelogs
  - patch
  - apply-patch
  - cherry-pick
  - rebase
    - make a feature foo on parent (parent--foo)
    - somebody changes the master branch to parent--bar
    - =git rebase foo bar= makes history parent--bar--foo
    - =rebase -i parent= lets me reorder, reword, edit in the middle of a history, basically modify history as you like
  - stash
  - blame
    - =--reverse= does this backwards from a given commit, so you can see who deleted/changed something relatie to that commit.
  - .gitignore
    - [[github.com/github/gitignore][github.com/github/gitignore]] contains good .gitignore files for various project types.
  - bisect
    - binary searches commits by building and debugging at pivots
* git internals
** git from the inside out
 [[https://codewords.recurse.com/issues/two/git-from-the-inside-out][link to blog post]]
** git from the bottom up
 [[https://jwiegley.github.io/git-from-the-bottom-up/][link to blog post]]
* screw ups
[[http://ohshitgit.com][link]]
* Data cleaning 
[[https://rtyley.github.io/bfg-repo-cleaner/][link]]
