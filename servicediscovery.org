* Separate modules of apps into separate apps
** Store them separately - called /microservices/
*** How do you get each service to know about the others?
** Tools
- DNS
- Google Chubby
- Apache Zookeeper
- Consul
* Consul
- Every service registers on the COnsul registry
- Services can read the services available on the registry
- Supports multiple datacenters out of the box
- Performs health checks
- Hierarchical key/value store that can be used for config
  - accessible with a HTTP API
