* History of databases
probably not important

* Relational databases
** ACID Properties
1. Atomicity
2. Consistency
3. Isolation
4. Durability

* MYSQL
** Protocol
- Half duplex
- tables or rows have mutexes

** Versions
It has a bunch of versions. Cluster edition is an in-memory DB.

* SQL hands-on
** SQL joins
https://i.stack.imgur.com/UI25E.jpg

** Cheat sheet
#+BEGIN_SRC
use dbname; # select which DB to use
show tables; # list all tables in current DB
select ... limit 40; # limit output to first 40 entries
select * from employees a join manager b on a.empno=b.empno; # inner join
#+END_SRC

* Scaling
** MySQL replication
Asynchronous update on slaves. Does not guarantee consistency.
** sharding
Break up DB wihtout redundant data.
*** vertical division
Database layout needs to be built to support this.
*** horizontal division
Used by mysql cluster. Database layout is unchanged.

* MongoDB
